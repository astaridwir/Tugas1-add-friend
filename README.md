# Tugas 1 PPW
## Member
- Astari Dwi Rahmanisa (1606917885)
- Muhammad Fakhrillah Abdul Azis (1606917531)
- Putri Rahma Arifa (1606917802)
- Zunino Sultan Anggara (1606917765)

## Herokuapp
Link : https://our-unique-app.herokuapp.com

## Pipeline status
![pipeline status](https://gitlab.com/fpvariel/TP1_PPW/badges/master/pipeline.svg)

## Coverage
![coverage report](https://gitlab.com/fpvariel/TP1_PPW/badges/master/coverage.svg)