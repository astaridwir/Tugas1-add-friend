from django.shortcuts import render

def index(request):
    response = {}
    response["friends"] = 120;
    response["feed"] = 23;
    return render(request, 'stats.html', response)
